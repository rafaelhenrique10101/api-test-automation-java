package services;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.*;

public class HttpMethods {

    public RequestSpecification request;
    URIs uri = new URIs();


    public HttpMethods(){
        this.request = given().baseUri(uri.baseUrl)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json");
    }

    public Response post(String resource, String payload) {
        return request.body(payload).when().post(resource + "?api_key=" + uri.apiKey).andReturn();
    }

    public Response get(String resource) {
        return request.when().get(resource + "?api_key=" + uri.apiKey).andReturn();
    }
}
