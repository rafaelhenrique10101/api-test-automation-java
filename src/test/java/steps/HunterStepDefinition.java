package steps;

import actions.LeadActions;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.pt.*;
import io.restassured.response.Response;
import models.Lead;
import org.junit.Assert;

import java.io.IOException;
import java.util.List;

public class HunterStepDefinition {
    public static Integer id;
    Lead lead = new Lead();
    LeadActions leadActions = new LeadActions(lead);
    List<List<String>> list;

    @Dado("que tenho as informacoes para criar uma nova lead")
    public void que_eu_tenho_as_informacoes_para_criar_uma_nova_lead(DataTable data) {

        list = data.asLists();

        lead.setEmail(list.get(0).get(1));
        lead.setFirstName(list.get(1).get(1));
        lead.setLastName(list.get(2).get(1));
        lead.setPosition(list.get(3).get(1));
        lead.setCompany(list.get(4).get(1));
    }

    @Quando("enviar uma requisicao do tipo {string} para {string}")
    public void eu_enviar_uma_requisicao_do_tipo_para(String method, String resource) {

        if(method.equals("POST")){
            leadActions.createLead(resource);
        }
        if (method.equals("GET")){
            leadActions.getLeads(resource + "/" + id);
        }

    }
    @Entao("recebo o codigo {string}")
    public void eu_receberei_o_codigo(String code) {

        String statusCode = String.valueOf(leadActions.getResponse().statusCode());

        try {
            Assert.assertEquals(code, statusCode);
        }catch (Exception e){
            Assert.fail("O status code retornado foi diferente do status code esperado: " + statusCode);
        }

    }
    @Entao("eu espero que a estrutura de response contenha os dados enviados para cadastro")
    public void eu_espero_que_a_estrutura_de_response_contenha_os_dados_enviados_para_cadastro() {

        boolean result = leadActions.validateLeadCreateResponse();
        Assert.assertTrue("Existem campos que estão com o formato dos dados inválidos", result);

    }

    //========================================================================================================

    @Entao("verei os dados cadastrados sao retornados")
    public void eu_verei_os_dados_do_cadastro_sao_retornados() {

        Response result = leadActions.getResponse();
        Assert.assertNotNull(result);
        result.print();
    }
}
