package actions;

import models.Lead;
import io.restassured.response.Response;
import org.json.JSONObject;
import steps.HunterStepDefinition;
import services.HttpMethods;

public class LeadActions {

    HttpMethods http = new HttpMethods();
    Response response;
    Lead lead;
    JSONObject payload = new JSONObject();
    JSONObject responseBody;
    JSONObject nodeData;

    public LeadActions(Lead lead){
        this.lead = lead;
    }

    public Response getResponse() {
        return response;
    }

    public void createLead(String resource) {
        try{
            JSONObject payload = mountLeadJSONPayload();
            response = http.post(resource, String.valueOf(payload));

            responseBody = new JSONObject(this.response.body().asString());
            nodeData = responseBody.getJSONObject("data");
            HunterStepDefinition.id = nodeData.getInt("id");

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public void getLeads(String resource) {
        try{
            response = http.get(resource);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public JSONObject mountLeadJSONPayload(){

        payload.put("email", lead.getEmail());
        payload.put("first_name", lead.getFirstName());
        payload.put("last_name", lead.getLastName());
        payload.put("position", lead.getPosition());
        payload.put("company", lead.getCompany());

        return payload;
    }

    public boolean validateLeadCreateResponse(){

        responseBody = new JSONObject(this.response.body().asString());
        nodeData = responseBody.getJSONObject("data");
        boolean result = false;

        String email = nodeData.getString("email");
        String firstName = nodeData.getString("first_name");
        String lastName = nodeData.getString("last_name");
        String company = nodeData.getString("company");

        return email.matches("^\\S+@\\S+\\.\\S+$")
                && firstName.matches("^\\w+$")
                && lastName.matches("^\\w+$")
                && company.matches("^\\w+$");
    }

    public boolean compareDataLeads(){
        responseBody = new JSONObject(this.response.body().asString());
        nodeData = responseBody.getJSONObject("data");
        boolean result = false;

        String email = nodeData.getString("email");
        String firstName = nodeData.getString("first_name");
        String lastName = nodeData.getString("last_name");
        String position = nodeData.getString("position");
        String company = nodeData.getString("company");

        return lead.getEmail().equals(email)
                && lead.getFirstName().equals(firstName)
                && lead.getLastName().equals(lastName)
                && lead.getPosition().equals(position)
                && lead.getCompany().equals(company);
    }
}
