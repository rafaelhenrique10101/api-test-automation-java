# language: pt
  Funcionalidade: Leads

    @criarLeads
    Cenario: Criar Lead
      Dado que tenho as informacoes para criar uma nova lead
        |email            |rafael.souza@gmail.com     |
        |first_name       |Rafael                     |
        |last_name        |Souza                      |
        |position         |QA Automation              |
        |company          |Accenture                  |

      Quando enviar uma requisicao do tipo "POST" para "/leads"
      Entao recebo o codigo "201"
      E eu espero que a estrutura de response contenha os dados enviados para cadastro

    @recuperarLeads
    Cenario: Recuperar Leads
      Quando enviar uma requisicao do tipo "GET" para "/leads"
      Entao recebo o codigo "200"
      E verei os dados cadastrados sao retornados
